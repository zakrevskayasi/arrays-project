package com.stormnet.arrays;

import java.util.Random;

public class Task8Class {
    private static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static void main(String[] args) {
        int[][] array = new int[5][];
        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            int length = getRandomNumberInRange(4, 15);
            array[i] = new int[length];
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = getRandomNumberInRange(1, 20);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }

        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < array[i].length; j++) {
                    sum += array[i][j];
                }
                System.out.println("Sum of array elements with index " + i + " is " + sum);
            }
        }

    }

}
