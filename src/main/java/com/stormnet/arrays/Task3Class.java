package com.stormnet.arrays;

import java.util.Random;

public class Task3Class {
    public static void main(String[] args) {
        Random rand = new Random();
        int evenNumCounter = 0;
        int[] array = new int[15];

        for(int i = 0; i < array.length; i++){
            array[i] = rand.nextInt(10);
            System.out.print(array[i] + " ");

            if(array[i] > 0 && array[i] % 2 == 0){
                evenNumCounter++;
            }
        }
        System.out.println("\nEven array elements count is " + evenNumCounter);
    }
}
