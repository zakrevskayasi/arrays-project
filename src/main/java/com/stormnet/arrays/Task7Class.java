package com.stormnet.arrays;

import java.util.Random;

public class Task7Class {
    private static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static void main(String[] args) {
        int[][] array = new int[5][];
        int sum = 0;
        int[] tmp = array[0];

        for (int i = 0; i < array.length; i++) {
            int length = getRandomNumberInRange(4, 15);
            array[i] = new int[length];
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = getRandomNumberInRange(1, 20);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }

        int firstArraylength = array[0].length;

        for (int i = 0; i < array.length; i++) {
            if (array[i].length < firstArraylength) {
                tmp = array[i];
                firstArraylength = array[i].length;
            }
        }

        for (int i = 0; i < tmp.length; i++) {
            sum += tmp[i];
        }

        System.out.println("The least array length is " + sum);
    }
}
