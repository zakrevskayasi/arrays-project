package com.stormnet.arrays;

public class Task2Class {
    public static void main(String[] args) {
        //Find array size
        int size = 0;
        for(int i = 1; i <= 99; i++){
            if(i % 2 != 0){
                size++;
            }
        }

        //Create, fill and print array
        int[] array = new int[size];
        for(int i = 1, j = 0; i <= 99; i++){
            if(i % 2 != 0){
                array[j] = i;
                System.out.print(array[j] + " ");
                j++;
            }
        }

        System.out.println("\n");

        //Print array in column
        for(int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }

    }
}
