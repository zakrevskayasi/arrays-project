package com.stormnet.arrays;

public class Task5Class {
    public static void main(String[] args) {
        double average1 = 0;
        double average2 = 0;

        //The first array
        int[] array1 = new int[5];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = (int) (Math.random() * 6);
            System.out.print(array1[i] + " ");
            average1 += (double)array1[i] / array1.length;
        }

        System.out.println();

        //The second array
        int[] array2 = new int[5];
        for (int i = 0; i < array2.length; i++) {
            array2[i] = (int) (Math.random() * 6);
            System.out.print(array2[i] + " ");
            array2[i] += (double)array2[i] / array2.length;
        }

        System.out.println();

        //Compare average values
        if(average1 == average2){
            System.out.println("Avearge values are equals for two arrays!");
        }else {
            if(average1 > average2){
                System.out.println("An average value of the first array is greater then the second!");
            }else {
                System.out.println("An average value of the second array is greater then the first!");
            }
        }


    }
}
